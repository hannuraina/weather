/*eslint no-unused-expressions: 0 */

import React from 'react';
import TestUtils from 'react-addons-test-utils';
import { expect } from 'chai';

import Body from '../components/Body/Body';

describe('Body', () => {
  let cities = [
    { id: 1, label: 'New York' },
    { id: 2, label: 'Chicago' }
  ];
  let formats = ['F', 'C'];
  let component = TestUtils.renderIntoDocument(<Body cities={cities} formats={formats} />);
  let bodyEl = React.findDOMNode(component);

  it('Should render the cities items to dropdown', () => {
    let dropdownElements = bodyEl.querySelectorAll('.dropdown-menu li');
    expect(dropdownElements.length).to.equal(cities.length);
  });

  it('Should render format buttons', () => {
    let formatButtons = bodyEl.querySelectorAll('.btn-group');
    expect(formatButtons.length).to.equal(formats.length);
  });
});
