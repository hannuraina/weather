import pkg from '../../package';

export const DEBUG = (process.env.NODE_ENV !== 'production');
export const APP_TITLE = pkg.name;

export const CITIES_GET_SUCCESS = 'CITIES_GET_SUCCESS';
export const CITIES_GET_ERROR = 'CITIES_GET_ERROR';
export const CITIES_UPDATED = 'CITIES_UPDATED';

export const WEATHER_GET_SUCCESS = 'WEATHER_GET_SUCCESS';
export const WEATHER_GET_ERROR = 'WEATHER_GET_ERROR';
export const WEATHER_UPDATED = 'WEATHER_UPDATED';
