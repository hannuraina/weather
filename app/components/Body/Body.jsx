import styles from './_Body.scss';

import React from 'react';
import { Button, ButtonGroup, DropdownButton, MenuItem } from 'react-bootstrap';
import AppActions from '../../actions/AppActions';
import WeatherStore from '../../stores/WeatherStore';
import Util from '../../util/Util';

let { PropTypes } = React;

export default class Body extends React.Component {

  state = { weather: null, city: null, format: null };
  static defaultProps = { cities: [], formats: []};
  static propTypes = { cities: PropTypes.array.isRequired, formats: PropTypes.array.isRequired };

  componentDidMount() {
    WeatherStore.addChangeListener(this.onStoreChange);
  }

  componentWillUnmount() {
    WeatherStore.removeChangeListener(this.onStoreChange);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ city: nextProps.cities[0].label, format: nextProps.formats[0] });
    AppActions.getWeather(nextProps.cities[0].label);
  }

  onStoreChange = () => {
    this.setState({ weather: WeatherStore.get(0) });
  }

  onFormatChange = (e) => {
    this.setState({ format: e.target.value });
  }

  onCityChange = (e, city) => {
    this.setState({ city: city });
    AppActions.getWeather(city);
  }

  render() {
    let weather = this.state.weather;
    let weatherIcon = weather ? weather.weather[0].icon : null;

    return (
      <div>
        { /* Settings */ }
        <ButtonGroup className={styles.buttonGroup}>

          {/* City Selection */}
          <DropdownButton title={this.state.city} >
            { !this.props.cities ? null : this.props.cities.map(city => {
              return (<MenuItem
                active={this.state.city === city.label}
                eventKey={city.label}
                key={city.id}
                onSelect={this.onCityChange}>{city.label}
              </MenuItem>);
            })}
          </DropdownButton>

          {/* Settings: C/F Selection */}
          { this.props.formats.map(format => {
            return (<Button
              active={this.state.format === format}
              key={format}
              onClick={this.onFormatChange}
              value={format}>{format}
            </Button>);
          })}

        </ButtonGroup>

        {/* Overview */}
        { !weather ? null : (
          <div className={styles.weather}>
            <div className={styles.content}>
              <div>{weather.weather[0].main}</div>
              <div>
                <span className="temperature">{Util.convertKelvin(weather.main.temp, this.state.format)}</span>&deg;
                <span className={styles.subscript}>
                  MIN: {Util.convertKelvin(weather.main.temp_min, this.state.format)}&deg;
                </span>
                <span className={styles.subscript}>
                  MAX: {Util.convertKelvin(weather.main.temp_max, this.state.format)}&deg;
                </span>
              </div>
              <div>Humidity: {weather.main.humidity}%</div>
              <div>Wind: {weather.wind.speed}mph</div>
            </div>
            {<img src={`//openweathermap.org/img/w/${weatherIcon}.png`}></img>}
          </div>)
        }
      </div>
    );
  }
}
