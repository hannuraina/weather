import styles from './_App.scss';

import React from 'react';
import AppActions from '../../actions/AppActions';
import CitiesStore from '../../stores/CitiesStore';
import Body from '../Body/Body';

export default class App extends React.Component {

  state = { cities: null };

  componentDidMount() {
    CitiesStore.addChangeListener(this.onStoreChange);
    AppActions.getCities();
  }

  componentWillUnmount() {
    CitiesStore.removeChangeListener(this.onStoreChange);
  }

  onStoreChange = () => {
    this.setState({cities: CitiesStore.getAll()});
  }

  render() {
    return (
      <div className={styles.app}>
        <Body cities={this.state.cities} formats={['F', 'C']}/>
      </div>
    );
  }
}
