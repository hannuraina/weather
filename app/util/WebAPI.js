import jsonp from 'jsonp-promise';
import query from 'query-string';

const APIKEY = 'b80a93213bf2ccb3776151b1938f9a97';
const APIBASE = '/data/2.5/weather';

export default {
  _cache: {},
  getCities() {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(['New York', 'Chicago', 'Houston', 'Seattle', 'San Diego'].map((item, i) => {
          return {
            id: i,
            label: item
          };
        }));
      }, 500);
    });
  },
  getWeather(city) {
    return this._cache[city] ?
      new Promise((resolve) => { resolve(this._cache[city]); }) :
      jsonp([APIBASE, query.stringify({APPID: APIKEY, q: city})].join('?')).promise;
  },
  cache(key, value) {
    this._cache[key] = value[0];
  }
};
