export default {
  convertKelvin(deg, format='F') {
    return format === 'C' ?
      Math.floor(deg - 273.15) :
      Math.floor((deg * (9 / 5)) - 459.67);
  }
};
