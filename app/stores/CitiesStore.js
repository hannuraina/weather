import BaseStore from './BaseStore';
import AppDispatcher from '../dispatcher/AppDispatcher';

import {
  CITIES_UPDATED,
  CITIES_GET_SUCCESS
} from '../constants/AppConstants';

class CitiesStore extends BaseStore {

  emitChange() {
    this.emit(CITIES_UPDATED);
  }

  addChangeListener(callback) {
    this.on(CITIES_UPDATED, callback);
  }

  removeChangeListener(callback) {
    this.removeListener(CITIES_UPDATED, callback);
  }
}

let store = new CitiesStore();

AppDispatcher.register((action) => {
  switch(action.actionType) {
    case CITIES_GET_SUCCESS:
      store.setAll(action.items);
      break;
    default:
  }
});

export default store;
