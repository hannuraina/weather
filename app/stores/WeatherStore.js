import BaseStore from './BaseStore';
import AppDispatcher from '../dispatcher/AppDispatcher';

import {
  WEATHER_UPDATED,
  WEATHER_GET_SUCCESS
} from '../constants/AppConstants';

class WeatherStore extends BaseStore {

  emitChange() {
    this.emit(WEATHER_UPDATED);
  }

  addChangeListener(callback) {
    this.on(WEATHER_UPDATED, callback);
  }

  removeChangeListener(callback) {
    this.removeListener(WEATHER_UPDATED, callback);
  }
}

let store = new WeatherStore();

AppDispatcher.register((action) => {
  switch(action.actionType) {
    case WEATHER_GET_SUCCESS:
      store.setAll([action.data]);
      break;
    default:
  }
});

export default store;
