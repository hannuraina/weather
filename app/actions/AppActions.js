import AppDispatcher from '../dispatcher/AppDispatcher';
import WebAPI from '../util/WebAPI';

import {
  CITIES_GET_SUCCESS,
  CITIES_GET_ERROR,
  WEATHER_GET_SUCCESS,
  WEATHER_GET_ERROR
} from '../constants/AppConstants';

export default {
  getCities() {
    WebAPI.getCities()
    .then((items) => {
      AppDispatcher.dispatch({
        actionType: CITIES_GET_SUCCESS,
        items: items
      });
    })
    .catch(() => {
      AppDispatcher.dispatch({
        actionType: CITIES_GET_ERROR
      });
    });
  },
  getWeather(city) {
    WebAPI.getWeather(city)
    .then((data) => {
      WebAPI.cache(city, data);
      AppDispatcher.dispatch({
        actionType: WEATHER_GET_SUCCESS,
        data: data
      });
    })
    .catch((e) => {
      console.log(e);
      AppDispatcher.dispatch({
        actionType: WEATHER_GET_ERROR
      });
    });
  }
};
