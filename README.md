## What

Create a simple website that will display the weather report for a
The website should have a list box (or some dynamic method) from
which a user can select the following city names (New York, Chicago,
Seattle, Houston, San Diego). By selecting a city a user should see the
weather report for that city: date, max and min temperature, pressure,
humidity etc. The website should be production deployable in terms
of design, code quality, testing, and deployment instructions. You can
use whichever Javascript framework/library you prefer (although React
or Angular would be preferred if you have no other preference).
Dress to impress… any additional flourishes to show a complex
technique technique or cool UI hack is encouraged. (We recognize it’s
a coding assignment and you don’t want to waste hours on it, but
showing something beyond the basics is encouraged.)

## Seed Project

https://github.com/badsyntax/react-seed

#demo

https://serene-chamber-30031.herokuapp.com/

## npm scripts

* `npm test` - Run tests
* `npm start` - Pack and serve (app running at http://localhost:8080)

## deploy to heroku

Requires https://toolbelt.heroku.com/

```bash
$ heroku config:set NODE_ENV=production
$ heroku config:set NPM_CONFIG_PRODUCTION=false
$ heroku login
$ heroku create
$ git push heroku master
$ heroku open
```
## troubleshooting heroku

If using ssh on your heroku you may have issues pushing your branch
to git. Run this command to fix the problem:

```bash
$ git remote rm heroku
$ git remote add heroku git@heroku.com:<heroku-instance-name>.git
$ git push heroku master

## License

Copyright (c) 2016 James McClure

MIT (http://opensource.org/licenses/MIT)
